import React from 'react'

const ReceiptHeader = () => {
  return (
    <div className="receipt__header">
      <span className="receipt__header__item__text">Ngày nhập</span>
      <span className="receipt__header__item__text">Danh mục</span>
      <span className="receipt__header__item__text">Số lượng tồn</span>
      <span className="receipt__header__item__text">Giá tiền</span>
      <span className="receipt__header__item__text">Người nhập</span>
      <span className="receipt__header__item__text"></span>
    </div>
  )
}

export default ReceiptHeader
