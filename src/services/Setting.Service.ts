import HttpService from "./getWays/Setting.GetWay";
import { ProductDocument } from "../interfaces/Product.Interface";
import { EnumURL, getTokenLocal } from "../utils/Common";
import axios from "axios";
import UserDocument from "interfaces/User.Interface";

export const CreateAccountForStaff = async (user: UserDocument) => {
  return await HttpService.post(EnumURL.staff, {
    firstName: user.firstName,
    lastName: user.lastName,
    address: user.address,
    phone: user.phone,
    email: user.email,
    password: user.password,
    cPassword: user.cPassword,
  });
};

export const Login = async (email: string, password: string) => {
  console.log('call login function api')
  return HttpService.post(EnumURL.login, {
    email: email,
    password: password,
  });
};

export const GetProfile = async () => {
  return await HttpService.get(EnumURL.profile);
};

//---CATEGORY
export const AddCategory = async (name: String) => {
  return await HttpService.post(`${EnumURL.category}`, {
    name: name,
  });
};

export const GetCategory = async (_id: String) => {
  return await HttpService.get(`${EnumURL.category}/${_id}`);
};

export const GetListCategories = async () => {
  return await HttpService.get(EnumURL.categories);
};

export const GetListCategorySorted = async (type: string) => {
  return await HttpService.post(EnumURL.sort.category, {
    type: type,
  });
};

export const EditCategory = async (_id: String, name: String) => {
  return await HttpService.put(`${EnumURL.category}/${_id}`, {
    name: name,
  });
};

export const DeleteCategory = async (_id: String) => {
  return await HttpService.delete(`${EnumURL.category}/${_id}`);
};

//---PRODUCTS
export const AddProduct = async (Product: ProductDocument) => {
  return await HttpService.post(EnumURL.product, {
    name: Product.name,
    price: Product.price,
    images: Product.images,
    description: Product.description,
    category: Product.category._id,
  });
};

export const GetLengthOfProduct = async () => {
  return await HttpService.get(`${EnumURL.products}/length`);
};

export const GetLengthOfProductWithCategory = async (category: string) => {
  return await HttpService.get(`${EnumURL.products}/length/${category}`);
};

export const GetProduct = async (_id: string) => {
  return await HttpService.get(`${EnumURL.product}/${_id}`);
};

export const GetListProducts = async () => {
  return await HttpService.get(EnumURL.products);
};

export const GetListProductWithPage = async (page: number) => {
  return await HttpService.get(`${EnumURL.products}/search/${page}`);
};

export const GetListProductSortByName = async (type: string) => {
  return await HttpService.post(EnumURL.sort.product.name, {
    type: type,
  });
};

export const GetListProductSortByNameWithPage = async (
  page: number,
  type: string
) => {
  return await HttpService.post(`${EnumURL.sort.product.name}/${page}`, {
    type: type,
  });
};

export const GetListProductSortedByPrice = async (type: string) => {
  return await HttpService.post(EnumURL.sort.product.price, {
    type: type,
  });
};

export const GetListProductSortedByPriceWithPage = async (
  page: number,
  type: string
) => {
  return await HttpService.post(`${EnumURL.sort.product.price}/${page}`, {
    type: type,
  });
};

export const GetListProductWithCategory = async (idCategory: string) => {
  return await HttpService.get(`${EnumURL.products}/${idCategory}`);
};

export const EditProduct = async (Product: ProductDocument) => {
  console.log(Product);
  return await HttpService.put(`${EnumURL.product}/${Product._id}`, {
    name: Product.name,
    price: Product.price,
    images: Product.images,
    description: Product.description,
    category: Product.category._id,
  });
};

export const DeleteProduct = async (_id: string) => {
  return await HttpService.delete(`${EnumURL.product}/${_id}`);
};

//---ORDER
export const GetListOrders = async (page: number) => {
  return await HttpService.get(`${EnumURL.orders}/${page}`);
};

export const GetListOrdersWithStatus = async (status: string) => {
  return await HttpService.get(`${EnumURL.orders}/status/${status}`);
};

export const UpdateStatusOrder = async (id: string, status: string) => {
  return await HttpService.post(`${EnumURL.orderUpdate}/${id}`, {
    status: status,
  });
};

export const OrderIsPaid = async (id: string) => {
  return await HttpService.post(`${EnumURL.order}/paid/${id}`);
};

export const GetListOrderSortedByTime = async (type: string, page: number) => {
  return await HttpService.post(`${EnumURL.order}/sorted`, {
    type: type,
    page: page,
  });
};

//Statistic
export const StatisticOrderByDay = async (dateString: string) => {
  return await HttpService.post(`${EnumURL.statistic}/day`, {
    dateString: dateString,
  });
};

export const StatisticOrderByMonth = async (year: number) => {
  return await HttpService.post(`${EnumURL.statistic}/month`, {
    year: year,
  });
};

export const StatisticOrderByYear = async () => {
  return await HttpService.get(`${EnumURL.statistic}/year`);
};

//Image
export const UploadImage = async (file: any, id: string) => {
  const fd = new FormData();
  fd.append("image", file);
  return await axios.post(`${EnumURL.baseUrl}/${EnumURL.image}/${id}`, fd, {
    headers: {
      "Content-Type": "multipart/form-data",
      Authorization: "Bearer " + getTokenLocal(),
    },
  });
};

export const UpdateImage = async (file: any, id: string) => {
  const fd = new FormData();
  fd.append("image", file);
  return await axios.put(`${EnumURL.baseUrl}/${EnumURL.image}/${id}`, fd, {
    headers: {
      "Content-Type": "multipart/form-data",
      Authorization: "Bearer " + getTokenLocal(),
    },
  });
};

//Receipt
export const GetListReceipt = async () => {
  return await HttpService.get(EnumURL.receipt);
};

export const GetReceiptByID = async (id: string) => {
  return await HttpService.get(`${EnumURL.receipt}/${id}`);
};

export const AddReceipt = async (
  quantityStock: number,
  price: number,
  categoryID: string
) => {
  return await HttpService.post(EnumURL.receipt, {
    quantityStock: quantityStock,
    price: price,
    category: categoryID,
  });
};

export const EditReceipt = async (
  id: string,
  quantityStock: number,
  price: number,
  categoryID: string
) => {
  return await HttpService.put(`${EnumURL.receipt}/${id}`, {
    quantityStock: quantityStock,
    price: price,
    category: categoryID,
  });
};

export const DeleteReceipt = async (id: string) => {
  return await HttpService.delete(`${EnumURL.receipt}/${id}`);
};
